from django.urls import reverse_lazy
from User.forms import LoginForm
from django.contrib.auth.models import User
from django.contrib.auth import (
    authenticate,
    login,
    logout
)
from django.shortcuts import (
    redirect,
    render
    )
from django.contrib.auth.decorators import login_required
from django.views import View
from django.views.generic.edit import (
    CreateView,
)


class CreateUser(CreateView):
    model = User
    fields = [
        'username',
        'first_name',
        'last_name',
        'email',
        'password'
        ]
    template_name = 'User/User_create.html'
    success_url = reverse_lazy('User:login_view')

    def post(self, request):
        data = request.POST
        user = User.objects.create_user(
            username=data["username"],
            first_name=data["first_name"],
            last_name=data["last_name"],
            email=data["email"],
            password=data["password"],
            is_active=False)
        user.set_password(data["password"])
        user.save()
        return redirect(self.success_url)


@login_required
def user_logout(request):
    logout(request)
    return redirect(reverse_lazy("User:login_view"))


class LoginView(View):
    template_name = "User/User_login.html"

    def get(self, request):
        if request.user.is_authenticated:
            return redirect(reverse_lazy("Project:list_project"))
        context = {"form": LoginForm()}
        return render(request, self.template_name, context)

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        print(user)
        if user:
            if user.is_active:
                login(request, user)
                return redirect(reverse_lazy('Project:list_project'))
        else:
            return redirect(reverse_lazy('User:login_view'))
