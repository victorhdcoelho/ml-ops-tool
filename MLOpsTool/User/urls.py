from django.conf.urls import url
from django.urls import path
from User.views import (
    CreateUser,
    LoginView,
    user_logout,
)

app_name = "User"
urlpatterns = [
    path('', LoginView.as_view(), name='login_view'),
    url(r'user/create/', CreateUser.as_view(), name='create_user'),
    url(r'user/logout/', user_logout, name='user_logout')
]
