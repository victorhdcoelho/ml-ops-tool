import pickle
from Pipeline.models import Pipeline
from tensorflow import keras
import numpy as np


class ModelService:
    def __init__(self, serializer, types):
        self.serializer = serializer
        self.model = None
        self.x = None
        self.y = None
        self.label = None
        self.real_model = None
        self.pipeline = None
        self.types = types

    def load_model(self, path):
        with open(path.replace('h5', 'pkl'), "rb") as f:
            self.model = pickle.load(f)

    def transform_axis(self, data, info):
        self.x = self.model['encoders'][info.x_field].transform([data])

    def predict(self, info, path=None):
        model = None
        if self.types == 'tf':
            model = keras.models.load_model(path)
            self.y = model.predict(self.x)
            self.y = np.argmax(self.y, axis=1)
        else:
            self.y = self.model['model'].predict(self.x)
        self.label = self.model['encoders'][info.y_field].inverse_transform(
            self.y
        )

    def run(self):
        self.pipeline = Pipeline.objects.get(id=self.serializer['pipeline_id'])
        self.real_model = self.pipeline.model
        self.load_model(self.real_model.file_pkl)
        self.transform_axis(self.serializer['data'], self.real_model)
        self.predict(self.real_model, self.real_model.file_pkl)
        return self.label
