from django.conf.urls import url
from . import views


app_name = "Model"

urlpatterns = [
    url(
        r"^model/create/(?P<id>\d+)/",
        views.CreateModel.as_view(),
        name="create_model"
    ),
    url(
        r"^model/predict/",
        views.PredictionView.as_view(),
        name="predict_model"
    ),
    url(
        r"model/api/(?P<id>\d+)/report/",
        views.PredictionReport.as_view(),
        name="predict_report"
    )
]
