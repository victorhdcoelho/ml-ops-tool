from django.db import models
from django.contrib.auth.models import User


class Model(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, unique=True)
    constructor = models.JSONField(null=True, blank=True)
    file_pkl = models.CharField(max_length=255, null=True, blank=True)
    x_field = models.CharField(max_length=255, default="text")
    y_field = models.CharField(max_length=255, default="label")
    usage = models.FloatField(default=0.0)
    errors = models.FloatField(default=0.0)
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} | {}".format(self.id, self.name)
