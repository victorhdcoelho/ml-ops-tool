import inspect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import (
    render,
    redirect
)
from django.views import generic
from .forms import CreateFormModel
from Pipeline.models import Pipeline
from .models import Model
from Data.models import Data, Row
from sklearn import svm
from sklearn import linear_model as linear
from tensorflow.keras import layers as tf
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import PredictionSerializer
from .services import ModelService


@method_decorator(login_required, name="dispatch")
class CreateModel(generic.View):
    template_name = "Model/Model_create.html"
    form_class = CreateFormModel

    def get(self, request, id):
        form = self.form_class()
        methods = {
            "svm": svm,
            "linear": linear,
            "tf": tf
        }
        new_methods = {}
        for key in methods.keys():
            new_methods[f"{key}_methods"] = inspect.getmembers(
                methods[key],
                inspect.isclass
            )
            new_methods[f"{key}_docs"] = [
                inspect.getdoc(getattr(methods[key], name)).replace(
                    '"', ''
                ).replace("'", "").replace('`', '').replace('\n', '<br>')
                if inspect.getdoc(
                    getattr(methods[key], name)
                ) is not None else ""
                for name, method in new_methods[f"{key}_methods"]
            ]
            new_methods[key] = zip(
                [each[0] for each in new_methods[f"{key}_methods"]],
                new_methods[f"{key}_docs"]
            )
        return render(
            request,
            self.template_name,
            {
                "form": form,
                **new_methods
            }
        )

    def post(self, request, **kwargs):
        id = kwargs.get('id')
        if request.POST['type_model'] == 'tf':
            methods = [request.POST[each] for each in request.POST.keys() if "func_method" in each] # noqa
            params = [request.POST[each] for each in request.POST.keys() if "params" in each] # noqa
        else:
            methods = request.POST['func_method']
            params = request.POST['params']
        data = {
            "name": request.POST['name'],
            "x_field": request.POST['x_axis'],
            "y_field": request.POST['y_axis'],
            "creator": request.user,
            "constructor": {
                "function": request.POST['type_model'],
                "method": methods,
                "params": params
            }
        }
        model = Model.objects.create(**data)
        Pipeline.objects.filter(pk=id).update(model=model)
        return redirect('Pipeline:detail_pipeline', pk=id)


class PredictionReport(APIView):
    def post(self, request, **kwargs):
        id = kwargs.get('id')
        pipe = Pipeline.objects.get(id=id)
        new_data = {
            "name": f"{pipe.data.name}_remake",
            "version": 1,
            "columns": pipe.data.columns,
            "creator": pipe.data.creator
        }
        data, create = Data.objects.get_or_create(**new_data)
        if create:
            rows = pipe.data.row_set.filter().values()
            new_rows = []
            for each in rows:
                each['id'] = None
                each['data_ref'] = data
                new_rows.append(Row(**each))
            Row.objects.bulk_create(new_rows)

        if request.data['corret'] is True:
            model = pipe.model
            pipe.current_metric = (model.usage - model.errors)/model.usage
            pipe.save()
            create_dict = {}
            for each in pipe.data.columns:
                create_dict[each] = None

            for key in create_dict.keys():
                try:
                    create_dict[key] = request.data['data'][key]
                except Exception:
                    pass
            list_data = [create_dict[key] for key in create_dict.keys()]
            Row.objects.create(content={"data": list_data}, data_ref=data)
        else:
            model = pipe.model
            model.errors += 1
            model.save()
            create_dict = {}
            pipe.current_metric = (model.usage - model.errors)/model.usage
            pipe.save()
            for each in pipe.data.columns:
                create_dict[each] = None

            for key in create_dict.keys():
                try:
                    create_dict[key] = request.data['data'][key]
                except Exception:
                    pass
            list_data = [create_dict[key] for key in create_dict.keys()]
            Row.objects.create(content={"data": list_data}, data_ref=data)
        return Response({"message": "success report"}, status.HTTP_200_OK)


class PredictionView(APIView):
    def post(self, request, **kwargs):
        serializer = PredictionSerializer(data=request.data)
        if serializer.is_valid():
            token = serializer.data['token']
            idx = serializer.data['pipeline_id']
            pipe = Pipeline.objects.get(id=idx)
            model = pipe.model
            model.usage += 1
            model.save()
            if token != pipe.project.token:
                return Response(
                    {"error": "Not Authorized!"},
                    status.HTTP_401_UNAUTHORIZED
                )
            service = ModelService(
                serializer.data, pipe.model.constructor['function']
            )
            label = service.run()
            data = {
                "api": f"/model/api/{serializer.data['pipeline_id']}/report/",
                "label": label,
                "f1_score": service.pipeline.metric,
            }
            return Response(
                data,
                status.HTTP_200_OK
            )
        return Response(
            {"error": serializer.errors},
            status.HTTP_400_BAD_REQUEST
        )
