from rest_framework import serializers


class PredictionSerializer(serializers.Serializer):
    pipeline_id = serializers.CharField(max_length=255)
    data = serializers.CharField()
    token = serializers.CharField(max_length=255)
