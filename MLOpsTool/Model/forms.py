from django import forms


CHOICES = [
    ("svm", "SVM"),
    ("tf", "Tensorflow"),
    ("linear", "Linear Models")
]


class CreateFormModel(forms.Form):
    name = forms.CharField()
    type_model = forms.ChoiceField(choices=CHOICES)

    def __init__(self, *args, **kwargs):
        super(CreateFormModel, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
