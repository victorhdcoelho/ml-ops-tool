#!/user/bin/env ash

cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
echo "America/Sao_Paulo" > /etc/timezone
exec python3 manage.py makemigrations &
exec python3 manage.py migrate &
exec echo "yes" | python manage.py collectstatic &
# exec python3 manage.py runserver 0.0.0.0:8000
exec gunicorn MLOpsTool.wsgi -b 0.0.0.0:$PORT --log-file -
