const uploadForm = document.getElementById('form-data')
const input = document.getElementById('id_data_file')
const progressBox = document.getElementById('progress-box')
const cancelBox = document.getElementById('cancel-box')
const cancelBtn = document.getElementById('cancel-button')
const csrf = document.getElementsByName('csrfmiddlewaretoken')

if(input !== null){
input.addEventListener('change', ()=>{
  progressBox.classList.remove('not-visible');
  cancelBox.classList.remove('not-visible');

  const df = input.files[0];

  const fd = new FormData()
  fd.append('csrfmiddlewaretoken', csrf[0].value);
  fd.append('df', df);
  console.log(fd)

  $.ajax({
    type: 'POST',
    url: uploadForm.action,
    enctype: 'multipart/form-data',
    data: fd,
    beforeSend: function(){
    
    },
    xhr: function(){
      const xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener('progress', e=>{
        if(e.lengthComputable){
          const percent = (e.loaded/e.total) * 100;
          console.log(percent)
          progressBox.innerHTML = `
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-success" 
                 role="progressbar" 
                 style="width: ${percent}%" 
                 aria-valuenow="${percent}" 
                 aria-valuemin="0" 
                 aria-valuemax="100"></div>
          </div>
        <p>${percent.toFixed(2)}%</p>`
        }
      })
      cancelBtn.addEventListener('click', ()=>{
        xhr.abort();
        progressBox.innerHTML=""
        cancelBox.classList.add('not-visible')
      })
      return xhr
    },
    success: function(response){
      console.log(response)
    },
    error: function(error){
      console.log(error)
    },
    cache: false,
    contentType: false,
    processData: false,
  }
  )
})
}
