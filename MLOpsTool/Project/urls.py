from django.conf.urls import url
from Project import views


app_name = "Project"

urlpatterns = [
    url(r'project/list/', views.ProjectListView.as_view(),
        name='list_project'),
    url(r'project/create/', views.ProjectCreateView.as_view(),
        name='create_project'),
    url(r'project/detail/(?P<pk>\w+)/', views.ProjectDetailView.as_view(),
        name='detail_project'),

]
