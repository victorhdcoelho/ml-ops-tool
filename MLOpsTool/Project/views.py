# from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import (
    redirect,
)
from django.views import generic
from Project import models as project
from Project import forms as project_forms
from uuid import uuid4


@method_decorator(login_required, name='dispatch')
class ProjectListView(generic.list.ListView):
    model = project.Project
    template_name = 'Project/Project_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectListView, self).get_context_data(**kwargs)
        context["PAGE_TITLE"] = "LISTA DE PROJETOS"
        context["URL"] = "/project/detail/"
        return context


@method_decorator(login_required, name='dispatch')
class ProjectCreateView(generic.CreateView):
    model = project.Project
    form_class = project_forms.ProjectForm
    template_name = 'Project/Project_create.html'
    success_url = reverse_lazy('Project:list_project')

    def post(self, request):
        token = uuid4().hex
        data = dict(
            name=request.POST["name"].replace(' ', '_'),
            description=request.POST["description"],
            creator=request.user,
            token=token
        )
        obj = project.Project.objects.create(**data)
        try:
            obj.project_tag.set(request.POST["project_tag"])
            obj.save()
        except Exception:
            obj.save()
        return redirect(reverse_lazy("Project:list_project"))


@method_decorator(login_required, name='dispatch')
class ProjectDetailView(generic.DetailView):
    model = project.Project
    template_name = "Project/Project_detail.html"
