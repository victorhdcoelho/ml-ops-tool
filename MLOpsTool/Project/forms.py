from django.forms.models import ModelForm
from django.forms.widgets import CheckboxSelectMultiple
from Project.models import Project


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = "__all__"
        exclude = ["num_models", "creator", "token"]
        widgets = {
            "project_tag": CheckboxSelectMultiple,
        }

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if visible.field.label == "Project tag":
                continue
            visible.field.widget.attrs['class'] = 'form-control'
