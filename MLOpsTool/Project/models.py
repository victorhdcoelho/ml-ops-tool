from django.db import models
from Tag.models import Tag
from django.contrib.auth.models import User


class Project(models.Model):
    name = models.CharField(max_length=255, primary_key=True)
    project_tag = models.ManyToManyField(
                Tag,
                blank=True
            )
    description = models.TextField(null=True)
    creation_date = models.DateTimeField(auto_now=True)
    update_field = models.DateTimeField(auto_now=True)
    num_models = models.IntegerField(default=0)
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    token = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)
