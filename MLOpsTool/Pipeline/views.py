from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import (
    redirect,
    render
)
from django.views import generic
from Pipeline.models import Pipeline
from Project.models import Project
from .forms import UpdateDataForm, UpdateThreshold
from Pipeline.services import PipelineService
# from Pipeline import forms as pipeline_forms


@login_required
def add_pipeline(request, **kwargs):
    project_name = kwargs.get('name')
    project = Project.objects.get(name=project_name)
    version = project.num_models + 1
    Pipeline.objects.create(
        version=version,
        creator=request.user,
        project=project
    )
    return redirect('Project:detail_project', pk=project_name)


@method_decorator(login_required, name="dispatch")
class PipelineDetail(generic.DetailView):
    model = Pipeline
    template_name = 'Pipeline/Pipeline_detail.html'


@method_decorator(login_required, name="dispatch")
class UpdateDataView(generic.View):
    template_name = "Pipeline/Pipeline_update_data.html"

    def get(self, request, **kwargs):
        form = UpdateDataForm()
        return render(request, self.template_name, {"form": form, **kwargs})

    def post(self, request, **kwargs):
        data_id = request.POST['data']
        pipeline = kwargs.get('pk')
        Pipeline.objects.filter(id=pipeline).update(data_id=data_id)
        return redirect('Pipeline:detail_pipeline', pk=pipeline)


@method_decorator(login_required, name="dispatch")
class UpdateThresholdView(generic.View):
    template_name = "Pipeline/Pipeline_update_threshold.html"

    def get(self, request, **kwargs):
        form = UpdateThreshold()
        return render(request, self.template_name, {"form": form})

    def post(self, request, **kwargs):
        data = {
            "f1_hi": request.POST['f1_hi'],
            "f1_med": request.POST['f1_med'],
            "f1_low": request.POST['f1_low'],
            "cut": request.POST['cut']
        }
        try:
            pipeline = Pipeline.objects.filter(pk=kwargs.get('id'))
            pipeline.update(**data)
            return redirect('Project:detail_project', pk=kwargs.get('pk'))
        except Exception:
            return redirect('Project:detail_project', pk=kwargs.get('pk'))


@login_required
def start_train(request, id):
    pipeline = Pipeline.objects.get(id=id)
    try:
        service = PipelineService(pipeline.id)
        service.run()
        return redirect('Project:detail_project', pk=pipeline.project.pk)
    except Exception as error:
        print(error)
        return redirect('Project:detail_project', pk=pipeline.project.pk)


@login_required
def delete_pipeline(request, id):
    pipeline = Pipeline.objects.get(id=id)
    project = pipeline.project
    if pipeline:
        project.num_models -= 1
        project.save()
        pipeline.delete()
        return redirect('Project:detail_project', pk=pipeline.project.pk)
    return redirect('Project:detail_project', pk=pipeline.project.pk)
