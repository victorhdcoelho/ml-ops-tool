import pandas as pd
from .models import Pipeline
from sklearn import svm
from sklearn import preprocessing
from sklearn import linear_model as linear
from sklearn.feature_extraction import text as sk
from tensorflow.keras import layers as tf
from tensorflow.keras.models import Sequential
from sklearn.metrics import f1_score
import numpy as np
import pickle


LIBS = {
    "SK": sk,
    "SK_LABEL": preprocessing,
    "SVM": svm,
    "LINEAR": linear,
    "TF": tf
}


class PipelineService:
    def __init__(self, pipeline):
        self.pipeline = Pipeline.objects.get(id=pipeline)
        self.preprocessings = {}
        self.instances = {}

    def prepare_data(self):
        columns = self.pipeline.data.columns
        data = self.pipeline.data.row_set.filter(
        ).values_list(
            'content__data',
            flat=True
        )
        index = [each for each in range(len(data))]
        df = pd.DataFrame(data, columns=columns, index=index)
        return df

    def get_params(self, params):
        if params == "":
            new_params = {}
        else:
            params = params.split(',')
            new_params = {}
            for each in params:
                args = each.split('=')
                if "'" in args[1] or '"' in args[1]:
                    new_params[args[0]] = args[1].replace(
                        '"', ''
                    ).replace("'", "").replace(' ', '')
                elif "." in args[1]:
                    new_params[args[0]] = float(args[1])
                else:
                    new_params[args[0]] = int(args[1])
        return new_params

    def split_data(self, df):
        train = df.sample(frac=self.pipeline.cut, random_state=200)
        test = df.drop(train.index)
        return train, test

    def preprocessing_data(self, df, train=True):
        self.preprocessings = {}
        pre_processings = self.pipeline.preprocessing_set.filter()
        for idx, each in enumerate(pre_processings):
            lib = LIBS[each.function['lib']]
            params = self.get_params(each.function['params'])
            method = getattr(lib, each.function['method'])
            method_instance = method(**params)
            if each.target not in self.preprocessings:
                data = df[each.target]
                self.preprocessings[each.target] = []
            else:
                data = self.preprocessings[each.target][idx-1]
            if train:
                self.instances[each.target] = method_instance
                self.instances[each.target] = self.instances[each.target].fit(
                    data
                )
                self.preprocessings[each.target].append(
                    self.instances[each.target].transform(data)
                )
            else:
                self.preprocessings[each.target].append(
                    self.instances[each.target].transform(data)
                )
        return self.preprocessings

    def save_model(self, model, info, instances=None):
        if info.constructor['function'] != 'tf':
            with open(f'pkls/{self.pipeline.project.name}_{self.pipeline.id}_{self.pipeline.version}.pkl', 'wb') as f: # noqa
                pickle.dump(model, f)
            return f'pkls/{self.pipeline.project.name}_{self.pipeline.id}_{self.pipeline.version}.pkl' # noqa
        else:
            model.save(f'pkls/{self.pipeline.project.name}_{self.pipeline.id}_{self.pipeline.version}.h5') # noqa
            with open(f'pkls/{self.pipeline.project.name}_{self.pipeline.id}_{self.pipeline.version}.pkl', 'wb') as f: # noqa
                pickle.dump(instances, f)
            return f'pkls/{self.pipeline.project.name}_{self.pipeline.id}_{self.pipeline.version}.h5' # noqa

    def train_others(self, model, x, y):
        model_method = getattr(
            LIBS[model.constructor['function'].upper()],
            model.constructor['method']
        )
        params = self.get_params(model.constructor['params'])
        model_instance = model_method(**params)
        hist = model_instance.fit(x, y)
        return hist

    def train_tf(self, model, x, y):
        network = Sequential()
        model_length = len(model.constructor['method'])
        layers = model.constructor['params'][:model_length]
        compile_args = model.constructor['params'][-1]
        fit_args = model.constructor['params'][-2]
        for function, param in zip(model.constructor['method'], layers):
            format_param = self.get_params(param)
            network.add(getattr(tf, function)(**format_param))
        compile_args = self.get_params(compile_args)
        fit_args = self.get_params(fit_args)
        compile_args['metrics'] = ['accuracy']
        network.compile(**compile_args)
        fit_args['x'] = x
        fit_args['y'] = y
        network.fit(**fit_args)
        return network

    def model_data(self, df, test):
        model = self.pipeline.model
        x = df[model.x_field][-1]
        y = df[model.y_field][-1]
        x_test = test[model.x_field][-1]
        y_test = test[model.y_field][-1]
        if model.constructor['function'] != 'tf':
            hist = self.train_others(model, x, y)
        else:
            hist = self.train_tf(model, x, y)
        preds = hist.predict(x_test)
        if model.constructor['function'] == 'tf':
            preds = np.argmax(preds, axis=1)
        score = f1_score(y_test, preds)
        self.pipeline.metric = round(score, 2)
        self.pipeline.save()
        response_model = {
            "model": hist,
            "encoders": self.instances
        }
        if model.constructor['function'] != 'tf':
            path = self.save_model(response_model, model)
        else:
            response_model = {
                "encoders": self.instances
            }
            path = self.save_model(hist, model, response_model)
        model.file_pkl = path
        model.save()

    def run(self):
        data = self.prepare_data()
        train, test = self.split_data(data)
        train_preprocessing = self.preprocessing_data(train)
        test_preprocessing = self.preprocessing_data(test, train=False)
        self.model_data(train_preprocessing, test_preprocessing)
