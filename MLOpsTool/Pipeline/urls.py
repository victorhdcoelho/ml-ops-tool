from django.conf.urls import url
from Pipeline import views


app_name = "Pipeline"

urlpatterns = [
    url(r'pipeline/create/(?P<name>\w+)/', views.add_pipeline,
        name='create_pipeline'),
    url(
        r'pipeline/detail/(?P<pk>\d+)/',
        views.PipelineDetail.as_view(),
        name="detail_pipeline"
       ),
    url(
        r'pipeline/update/data/(?P<pk>\d+)/',
        views.UpdateDataView.as_view(),
        name="update_data"
    ),
    url(
        r'pipeline/start/(?P<id>\d+)/',
        views.start_train,
        name="start_train"
    ),
    url(
        r'pipeline/delete/(?P<id>\d+)/',
        views.delete_pipeline,
        name="delete_pipeline"
    ),
    url(
        r'pipeline/update/threshold/(?P<pk>\w+)/(?P<id>\d+)/',
        views.UpdateThresholdView.as_view(),
        name="update_threshold"
    )
]
