from django.forms.models import ModelForm
from Pipeline.models import Pipeline


class ProjectForm(ModelForm):
    class Meta:
        model = Pipeline
        fields = ["version"]

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class UpdateDataForm(ModelForm):
    class Meta:
        model = Pipeline
        fields = ["data"]

    def __init__(self, *args, **kwargs):
        super(UpdateDataForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class UpdateThreshold(ModelForm):
    class Meta:
        model = Pipeline
        fields = ["f1_hi", "f1_med", "f1_low", 'cut']

    def __init__(self, *args, **kwargs):
        super(UpdateThreshold, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
