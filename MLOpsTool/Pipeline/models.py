from django.db import models
from Data.models import Data
from Model.models import Model
from Project.models import Project
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save


class Pipeline(models.Model):
    class Meta:
        unique_together = (("id", "version"))
    id = models.AutoField(primary_key=True)
    data = models.ForeignKey(
                Data,
                on_delete=models.CASCADE,
                null=True,
                blank=True,
            )
    model = models.ForeignKey(
                Model,
                on_delete=models.CASCADE,
                null=True,
                blank=True,
            )
    project = models.ForeignKey(
                Project,
                on_delete=models.CASCADE,
                null=True,
                blank=True
            )
    version = models.IntegerField()

    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    metric = models.FloatField(default=0.0)
    current_metric = models.FloatField(default=0.0)
    f1_hi = models.FloatField(default=1.0)
    f1_med = models.FloatField(default=0.66)
    f1_low = models.FloatField(default=0.33)
    cut = models.FloatField(default=0.8)

    def __str__(self):
        return "{} | {}".format(self.id, self.version)


@receiver(post_save, sender=Pipeline)
def update_num_models(sender, instance, **kwargs):
    project = Project.objects.get(name=instance.project)
    project.num_models = Pipeline.objects.filter(
        project=instance.project
    ).count()
    project.save()
