from django.db import models
from django.contrib.auth.models import User


class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True)
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{}".format(self.name)
