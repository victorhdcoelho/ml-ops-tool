from django.forms.models import ModelForm
from .models import Tag


class TagForm(ModelForm):
    class Meta:
        model = Tag
        fields = "__all__"
        exclude = ["creator"]

    def __init__(self, *args, **kwargs):
        super(TagForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
