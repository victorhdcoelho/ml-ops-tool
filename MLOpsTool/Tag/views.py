from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import generic
from .forms import TagForm
from . import models as tag
from django.shortcuts import redirect


@method_decorator(login_required, name="dispatch")
class TagCreateView(generic.CreateView):
    model = tag.Tag
    form_class = TagForm
    template_name = 'Tag/Tag_create.html'
    success_url = reverse_lazy('Tag:tag_list')

    def post(self, request):
        data = dict(
            name=request.POST["name"],
            description=request.POST["description"],
            creator=request.user
        )
        tag.Tag.objects.create(**data)
        return redirect(self.success_url)


@method_decorator(login_required, name="dispatch")
class TagListView(generic.list.ListView):
    model = tag.Tag
    template_name = 'Tag/Tag_list.html'

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        context["PAGE_TITLE"] = "LISTA DE TAGS"
        context["URL"] = "/tag/detail/"
        return context


@method_decorator(login_required, name="dispatch")
class TagDetailView(generic.DetailView):
    model = tag.Tag
    template_name = "Tag/Tag_detail.html"
