from django.conf.urls import url
from . import views

app_name = "Tag"

urlpatterns = [
    url(r'tag/list/', views.TagListView.as_view(), name="tag_list"),
    url(r'tag/detail/(?P<pk>\d+)/', views.TagDetailView.as_view(),
        name="tag_detail"),
    url(r'tag/create/', views.TagCreateView.as_view(), name="tag_create"),
]
