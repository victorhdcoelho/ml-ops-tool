from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import generic
from .forms import DataForm
from . import models as data
from django.shortcuts import redirect
import pandas as pd


VALID_DATA = {
    "csv": pd.read_csv,
    "json": pd.read_json,
    "parquet": pd.read_parquet,
    "xlsx": pd.read_excel,
}


@method_decorator(login_required, name="dispatch")
class DataCreateView(generic.CreateView):
    model = data.Data
    form_class = DataForm
    template_name = 'Data/Data_create.html'
    success_url = reverse_lazy('Data:data_list')

    def save_rows(self, rows, data_ref):
        list_rows = [
            data.Row(
                content={
                    "data": row
                },
                data_ref=data_ref
            ) for row in rows["data"]
        ]
        data.Row.objects.bulk_create(list_rows)

    def post(self, request):
        type_file = request.FILES["data_file"]._name.split(".")[-1]
        df = VALID_DATA[type_file](request.FILES["data_file"])
        df = df.dropna()
        payload = dict(
            name=request.POST["name"],
            version=request.POST["version"],
            columns=df.columns.to_list(),
            creator=request.user
        )
        data_ref = data.Data.objects.create(**payload)
        rows = df.to_dict('split')
        self.save_rows(rows, data_ref)
        return redirect(self.success_url)


@method_decorator(login_required, name="dispatch")
class DataListView(generic.list.ListView):
    model = data.Data
    template_name = 'Data/Data_list.html'


@method_decorator(login_required, name="dispatch")
class DataDetailView(generic.DetailView):
    model = data.Data
    template_name = "Data/Data_detail.html"

    def get_context_data(self, **kwargs):
        limit = self.request.GET.get('limit', '10')
        context = super(DataDetailView, self).get_context_data(**kwargs)
        data = self.model.objects.get(id=context["object"].id)
        context["row_set"] = data.row_set.all()[:int(limit)]
        return context
