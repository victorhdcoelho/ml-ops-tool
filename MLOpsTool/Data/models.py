from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField


class Data(models.Model):
    class Meta:
        unique_together = (('name', 'version'),)
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    version = models.IntegerField()
    columns = ArrayField(models.CharField(max_length=255, blank=True))
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} | {}".format(self.name, self.version)


class Row(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.JSONField()
    data_ref = models.ForeignKey(
        Data,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} | {}".format(self.id, self.data_ref)
