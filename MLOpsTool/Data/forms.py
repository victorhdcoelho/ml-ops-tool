from django.forms.models import ModelForm
from django import forms
from .models import Data


class DataForm(ModelForm):
    data_file = forms.FileField()

    class Meta:
        model = Data
        fields = "__all__"
        exclude = ["creator", "columns", "content"]

    def __init__(self, *args, **kwargs):
        super(DataForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
