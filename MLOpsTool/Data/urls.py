from django.conf.urls import url
from . import views

app_name = "Data"

urlpatterns = [
    url(r'data/list/', views.DataListView.as_view(), name="data_list"),
    url(r'data/detail/(?P<pk>\w+)/', views.DataDetailView.as_view(),
        name="data_detail"),
    url(r'data/create/', views.DataCreateView.as_view(), name="data_create"),
]
