import inspect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import (
    render,
    redirect
)
from django.views import generic
from .models import PreProcessing
from .forms import CreateFormPreProcessing
from Pipeline.models import Pipeline
from sklearn import preprocessing as label
from sklearn.feature_extraction import text as sk
from tensorflow.keras import layers as tf
from Pipeline.services import PipelineService


@method_decorator(login_required, name="dispatch")
class CreatePreProcessing(generic.View):
    model = PreProcessing
    template_name = "PreProcessing/PreProcessing_create.html"
    form_class = CreateFormPreProcessing

    def get(self, request, id):
        form = self.form_class()
        sklearn_methods = inspect.getmembers(sk, inspect.isclass)
        tensorflow_methods = inspect.getmembers(tf, inspect.isclass)
        label_methods = inspect.getmembers(label, inspect.isclass)
        sk_docs = [
            inspect.getdoc(getattr(sk, name)).replace(
                '"', ''
            ).replace("'", "").replace('`', '').replace('\n', '<br>')
            if inspect.getdoc(getattr(sk, name)) is not None else ""
            for name, method in sklearn_methods
        ]
        label_docs = [
            inspect.getdoc(getattr(label, name)).replace(
                '"', ''
            ).replace("'", "").replace('`', '').replace('\n', '<br>')
            if inspect.getdoc(getattr(label, name)) is not None else ""
            for name, method in label_methods
        ]
        tensorflow_docs = [
            inspect.getdoc(getattr(tf, name)).replace(
                '"', ''
            ).replace("'", "").replace('`', '').replace('\n', '<br>')
            if inspect.getdoc(getattr(tf, name)) is not None else ""
            for name, method in tensorflow_methods
        ]
        return render(
            request,
            self.template_name,
            {
                "form": form,
                "sk": zip([each[0] for each in sklearn_methods], sk_docs),
                "tf": zip(
                    [each[0] for each in tensorflow_methods],
                    tensorflow_docs
                ),
                "label": zip(
                    [each[0] for each in label_methods],
                    label_docs
                )
            }
        )

    def post(self, request, **kwargs):
        id = kwargs.get('id')
        print(request.POST)
        data = {
            "function": {
                "lib": request.POST['type_preprocessing'],
                "method": request.POST['func_method'],
                "params": request.POST['params']
            },
            "target": request.POST['target'],
            "name": request.POST['name'],
            "creator": request.user,
            "pipeline": Pipeline.objects.get(id=id)
        }
        PreProcessing.objects.create(**data)
        return redirect('Pipeline:detail_pipeline', pk=id)


def start_train(request, id):
    pipeline = Pipeline.objects.get(id=id)
    try:
        service = PipelineService(pipeline.id)
        service.run()
        return redirect('Project:detail_project', pk=pipeline.project.pk)
    except Exception:
        return redirect('Project:detail_project', pk=pipeline.project.pk)


@login_required
def delete_preprocessing(request, id):
    preprocessing = PreProcessing.objects.get(id=id)
    if preprocessing:
        preprocessing.delete()
        return redirect(
            'Pipeline:detail_pipeline', pk=preprocessing.pipeline.pk
        )
    return redirect(
        'Pipeline:detail_pipeline', pk=preprocessing.pipeline.pk
    )
