from django.db import models
from django.contrib.auth.models import User
from Pipeline.models import Pipeline


class PreProcessing(models.Model):
    id = models.AutoField(primary_key=True)
    function = models.JSONField()
    name = models.CharField(max_length=255)
    target = models. CharField(max_length=255)
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    pipeline = models.ForeignKey(
        Pipeline,
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return "{}| {}".format(self.id, self.name)
