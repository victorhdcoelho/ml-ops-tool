from django import forms


CHOICES = [
    ("SK", "Sklearn"),
    ("TF", "Tensorflow"),
    ("SK_LABEL", "SKlearnLabel"),
    ("CS", "Custom")
]


class CreateFormPreProcessing(forms.Form):
    name = forms.CharField()
    type_preprocessing = forms.ChoiceField(choices=CHOICES)

    def __init__(self, *args, **kwargs):
        super(CreateFormPreProcessing, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
