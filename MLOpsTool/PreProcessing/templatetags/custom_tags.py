from django import template


register = template.Library()


@register.filter(name="get_fill")
def get_fill(value, arg):
    for method, doc in value:
        if method == arg:
            return doc

    return ""
