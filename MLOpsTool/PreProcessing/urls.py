from django.conf.urls import url
from PreProcessing import views


app_name = "PreProcessing"

urlpatterns = [
    url(
        r'^PreProcessing/create/(?P<id>\d+)/',
        views.CreatePreProcessing.as_view(),
        name="create_preprocessing"
    ),
    url(
        r'^preprocessing/delete/(?P<id>\d+)/',
        views.delete_preprocessing,
        name="delete_preprocessing"
    )
]
