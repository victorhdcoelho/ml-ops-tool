"""MLOpsTool URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("User.urls", namespace='User')),
    path('', include("Tag.urls", namespace='Tag')),
    path('', include("Data.urls", namespace='Data')),
    path('', include("Pipeline.urls", namespace='Pipeline')),
    path('', include("Model.urls", namespace='Model')),
    path('', include("PreProcessing.urls", namespace='PreProcessing')),
    path('', include("Project.urls", namespace='Project'))
]
